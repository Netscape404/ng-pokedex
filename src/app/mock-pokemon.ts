import {Pokemon} from './pokedex/models/pokemon';

export const POKELIST: Pokemon[] = [
  {
    id: 1,
    name: 'Bulbizarre',
    types: [{ name: 'Plante' }],
    abilities: [
      {
        name: 'Chlorophylle',
        description: ''
      },
      {
        name: 'Engrais',
        description: ''
      }
    ],
    moves: [
      {
        name:'Charge',
        type:[{ name: 'Normal' }],
        power: 40,
        accuracy: 100,
        description: ''
      },
      {
        name:'Rugissement',
        type:[{ name: 'Normal' }],
        power: 0,
        accuracy: 100,
        description: ''
      },
      {
        name:'Fouet lianes',
        type:[{ name: 'Plante' }],
        power: 45,
        accuracy: 100,
        description: ''
      },
    ],
    generation: 1,
    description: '',
    height: 0.7,
    weight: 6.9,
    catchRate: 45
  },

  {
    id: 4,
    name: 'Salamèche',
    types: [{ name: 'Feu' }],
    abilities: [
      {
        name: 'Brazier',
        description: ''
      },
      {
        name: 'Force soleil',
        description: ''
      }
    ],
    moves: [
      {
        name:'Griffe',
        type:[{ name: 'Normal' }],
        power: 40,
        accuracy: 100,
        description: ''
      },
      {
        name:'Rugissement',
        type:[{ name: 'Normal' }],
        power: 0,
        accuracy: 100,
        description: ''
      },
      {
        name:'Flammèche',
        type:[{ name: 'Feu' }],
        power: 40,
        accuracy: 100,
        description: ''
      },
    ],
    generation: 1,
    description: '',
    height: 0.6,
    weight: 8.5,
    catchRate: 45
  },

  {
    id: 7,
    name: 'Carapuce',
    types: [{ name: 'Eau' }],
    abilities: [
      {
        name: 'Cuvette',
        description: ''
      },
      {
        name: 'Torrent',
        description: ''
      }
    ],
    moves: [
      {
        name:'Charge',
        type:[{ name: 'Normal' }],
        power: 40,
        accuracy: 100,
        description: ''
      },
      {
        name:'Mimi-Queue',
        type:[{ name: 'Normal' }],
        power: 0,
        accuracy: 100,
        description: ''
      },
      {
        name:'Ecume',
        type:[{ name: 'Eau' }],
        power: 40,
        accuracy: 100,
        description: ''
      },
    ],
    generation: 1,
    description: '',
    height: 0.5,
    weight: 9,
    catchRate: 45
  }
];
