import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PokedexRoutingModule} from './pokedex-routing.module';

import {PokemonComponent} from './containers/pokemon/pokemon.component';
import {PokemonInfoComponent} from './containers/pokemon-info/pokemon-info.component';
import {BasicPokemonInfoComponent} from './components/basic-pokemon-info/basic-pokemon-info.component';
import {PokemonProfileComponent} from './components/pokemon-profile/pokemon-profile.component';
import {SearchFilterPipe} from './pipes/search-filter-pipe';
import {FormsModule} from '@angular/forms';
import {MaterialModule} from '../shared/material.module';

@NgModule({
  declarations: [
    PokemonComponent,
    PokemonInfoComponent,
    BasicPokemonInfoComponent,
    PokemonProfileComponent,
    SearchFilterPipe,
  ],
  imports: [
    CommonModule,
    PokedexRoutingModule,
    MaterialModule,
    FormsModule
  ],
  exports: [
    MaterialModule
  ]
})
export class PokedexModule { }
