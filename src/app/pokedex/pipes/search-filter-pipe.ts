import {Pipe, PipeTransform} from '@angular/core';
import {Pokemon} from '../models/pokemon';

@Pipe({ name: 'filter' })
export class SearchFilterPipe implements PipeTransform {

  transform(pokemon: Pokemon[], searchInput: string): Pokemon[] {
    if (!pokemon || !searchInput) {
      return pokemon;
    }

    return pokemon.filter(pokemon =>
      pokemon.name.toLowerCase().indexOf(searchInput.toLowerCase()) !== -1);
  }
}
