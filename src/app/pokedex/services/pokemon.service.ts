import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';

import {POKELIST} from '../../mock-pokemon';
import {Pokemon} from '../models/pokemon';


@Injectable({ providedIn: 'root' })
export class PokemonService {
  getPokemon(id: number): Observable<Pokemon>{
    return of(POKELIST.find(pokemon => pokemon.id === id));
  }
}
