import {Types} from './types';
import {Abilities} from './abilities';
import {Moves} from './moves';

export interface Pokemon {
  id: number;
  name: string;
  types: Array<Types>;
  abilities: Array<Abilities>;
  moves: Array<Moves>;
  generation: number;
  description: string;
  height: number;
  weight: number;
  catchRate: number;
}
