import {Types} from './types';

export interface Moves {
  name: string;
  type: Array<Types>;
  power: number;
  accuracy: number;
  description: string;
}
