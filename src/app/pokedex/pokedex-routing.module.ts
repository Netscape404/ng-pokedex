import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PokemonComponent} from './containers/pokemon/pokemon.component';
import {PokemonInfoComponent} from './containers/pokemon-info/pokemon-info.component';

const routes: Routes = [
  { path: 'pokemon/:id', component: PokemonInfoComponent },
  {path: '', component: PokemonComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PokedexRoutingModule { }
