import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';

import {PokemonService} from '../../services/pokemon.service';
import {Pokemon} from '../../models/pokemon';
import {Subscription} from 'rxjs';

@Component({
  selector: 'pkd-pokemon-info',
  templateUrl: './pokemon-info.component.html',
  styleUrls: ['./pokemon-info.component.scss']
})
export class PokemonInfoComponent implements OnInit, OnDestroy {
  ngOnInit(): void {
    this.getPokemon();
  }

  private subscription: Subscription;
  private pokemon: Pokemon;

  constructor(
    private route: ActivatedRoute,
    private pokemonService: PokemonService,
    private location: Location
  ) { }

  getPokemon(): void {
    const id = + this.route.snapshot.paramMap.get('id');
    this.subscription = this.pokemonService.getPokemon(id)
      .subscribe(pokemon => this.pokemon = pokemon);
  }

  goBack(): void {
    this.location.back();
  }

  ngOnDestroy(): void {
    if (this.subscription)
      this.subscription.unsubscribe();
  }
}
