import {Component} from '@angular/core';
import {POKELIST} from '../../../mock-pokemon';

@Component({
  selector: 'pkd-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.scss']
})
export class PokemonComponent {
  pokelist = POKELIST;
  searchInput: string;
}
