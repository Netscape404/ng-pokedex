import {Component, Input} from '@angular/core';
import {Pokemon} from '../../models/pokemon';

@Component({
  selector: 'pkd-basic-pokemon-info',
  templateUrl: './basic-pokemon-info.component.html',
  styleUrls: ['./basic-pokemon-info.component.scss']
})
export class BasicPokemonInfoComponent {
  @Input() pokemon: Pokemon;
}
