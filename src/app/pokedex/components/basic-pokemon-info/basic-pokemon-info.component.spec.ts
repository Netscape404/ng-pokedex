import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BasicPokemonInfoComponent} from './basic-pokemon-info.component';

describe('BasicPokemonInfoComponent', () => {
  let component: BasicPokemonInfoComponent;
  let fixture: ComponentFixture<BasicPokemonInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicPokemonInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicPokemonInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
