import {Component, Input} from '@angular/core';
import {Pokemon} from '../../models/pokemon';

@Component({
  selector: 'pkd-pokemon-profile',
  templateUrl: './pokemon-profile.component.html',
  styleUrls: ['./pokemon-profile.component.scss']
})
export class PokemonProfileComponent {
  @Input() pokemon: Pokemon;
}
